package com.brandroid.twolinkedlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TwoLinkedList listInt = new TwoLinkedList();
        listInt.addNewItem("first");
        listInt.addNewItem(1);
        listInt.addNewItem("third");
        listInt.addNewItem(3.88);
        listInt.addNewItem("fifth");
        listInt.addNewItem("sixth");

        listInt.printAll();


        listInt.printValue(2);
        listInt.printValue(5);
        listInt.printValue(10);

        listInt.addNewItemToPosition("new1", 0);
        listInt.addNewItemToPosition("last", 5);
        listInt.addNewItemToPosition("new2", 3);
        listInt.addNewItemToPosition("new3", 10);

        listInt.printAll();

    }
}
