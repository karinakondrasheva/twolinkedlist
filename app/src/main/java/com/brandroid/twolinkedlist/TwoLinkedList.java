package com.brandroid.twolinkedlist;



public class TwoLinkedList<T> {

    private TwoLinkedList start;
    private TwoLinkedList end;
    private int size;

    private T value;
    private TwoLinkedList next;
    private TwoLinkedList prev;


    public TwoLinkedList() {}

    public void addNewItem(T newItem) {
        TwoLinkedList item = new TwoLinkedList();
        if (size == 0) {
            start = item;
        } else {
            end.next = item;
            item.prev = end;
        }
        item.value = newItem;
        end = item;
        size++;
    }

    public void addNewItemToPosition (T newItem, int position) {

        if (size == 0 || position == size) {
            addNewItem(newItem);
        }

        TwoLinkedList item = new TwoLinkedList();
        TwoLinkedList temp = start;

        if (position < 0 && position > size) {
            System.out.println("INCORRECT POSITION");
        } else {
            if (position == 0) {
                item.next = start;
                start.prev = item;
                item.value = newItem;
                start = item;
            } else {
                if (position > 0 && position < size - 1) {
                    {
                        while (position != 0) {
                            temp = temp.next;
                            position--;
                        }
                        item.prev = temp.prev;
                        item.next = temp;
                        item.value = newItem;
                        temp = item.prev;
                        temp.next = item;
                    }
                }
            }
            size++;
        }

    }

    public void printAll() {
        TwoLinkedList temp = start;
        while (temp != null) {
            System.out.println(temp.value + " ");
            temp = temp.next;
        }
    }

    public void printValue(int position) {
        TwoLinkedList temp = start;
        if (position >= 0 && position < size) {
            while (position != 0) {
                temp = temp.next;
                position--;

            }
            System.out.println(temp.value);
        }
        else System.out.println("INCORRECT POSITION)");

    }
}

